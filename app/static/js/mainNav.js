const navContainer = document.querySelector('.main-nav');
const navToggle = navContainer.querySelector('.main-nav__toggle');

navToggle.addEventListener('click', () => {
  document.body.classList.toggle('no-scroll');
  navContainer.classList.toggle('active');
});