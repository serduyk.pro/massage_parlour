const selectedDay = document.querySelector('input[name="day"]:checked').querySelector("time").getAttribute("datetime");
const selectedTime = document.querySelector('input[name="time"]:checked').parentElement.querySelector("time").getAttribute("datetime");

const selectedDateTime = `${selectedDay}T${selectedTime}`;
