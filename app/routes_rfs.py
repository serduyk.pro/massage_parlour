# app/routes.py
from flask import Blueprint, render_template, request

rfs_routes = Blueprint('rfs_routes', __name__)

# Страница "Запись"
@rfs_routes.route("/appointment", methods=['POST', 'GET'])
def appointment():
    if request.method == 'POST':
        name = request.form['name']
        number = request.form['number']
        mail = request.form['mail']
        service = request.form['service']
        dateservice = datetime.strptime(request.form['dateservice'],
                                        '%Y-%m-%dT%H:%M')

        client = Registration_for_services(name=name, number=number, mail=mail,
                                           service=service, dateservice=dateservice)

        try:
            db.session.add(client)
            db.session.commit()
            return redirect('/successfully')

        except Exception as e:
            return (str(e))

    else:
        return render_template('appointment.html')


# Страница "Вы успешно записанны"
@rfs_routes.route("/successfully")
def successfully():
    return render_template('successfully.html')