# app/models.py
from datetime import datetime
from app.__init__ import db, bcrypt

# Модель записи на услугу


class Registration_for_services(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    number = db.Column(db.String(15), nullable=False)
    mail = db.Column(db.String(100), nullable=True)
    service = db.Column(db.Text, nullable=False)
    dateservice = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return f"Registration_for_services('{self.name}', '{self.number}', '{self.mail}' '{self.service}', '{self.dateservice}')"

# Модель Auth


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    # Добавим поле для хранения времени регистрации
    registered_on = db.Column(
        db.DateTime, nullable=False, default=datetime.utcnow)

    def __init__(self, username, email, password):
        self.username = username
        self.email = email
        self.password = bcrypt.generate_password_hash(password).decode('utf-8')

    def check_password(self, password):
        return bcrypt.check_password_hash(self.password, password)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}')"
